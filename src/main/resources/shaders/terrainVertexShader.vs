#version 400 core

in vec3 position;
in vec2 textureCoords;
in vec3 normal;

out vec2 passTextureCoords;
out vec3 surfaceNormal;
out vec3 toLightVector;
out float visibility;

uniform mat4 transMatrix;
uniform mat4 projMatrix;
uniform mat4 viewMatrix;
uniform vec3 lightPosition;

const float fogDensity = 0.000;//0.007
const float fogGradient = 1; //5

void main(void){

    vec4 worldPosition = transMatrix * vec4(position, 1.0);
    vec4 positionRelativeToCam = viewMatrix* worldPosition;
    gl_Position = projMatrix * positionRelativeToCam;
    passTextureCoords = textureCoords;

    surfaceNormal = (transMatrix * vec4(normal, 0.0)).xyz;
    toLightVector = lightPosition - worldPosition.xyz;

    float distance = length(positionRelativeToCam.xyz);
    visibility = exp(-pow((distance*fogDensity), fogGradient));
    visibility = clamp(visibility, 0.0,1.0);
}