package cz.filek.zpg.engine;

import cz.filek.zpg.math.Maths;
import cz.filek.zpg.math.Matrix4f;
import cz.filek.zpg.math.Vector3f;
import cz.filek.zpg.models.RawModel;
import cz.filek.zpg.shaders.TerrainShader;
import cz.filek.zpg.terrains.Terrain;
import cz.filek.zpg.textures.TerrainTexturePack;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;

import java.util.List;

/**
 * @author Filip Jani - 5. 11. 2016.
 *         <p>
 *         Třída zajišťující renderování terénu
 */
public class TerrainRenderer {
    private TerrainShader terrainShader;

    /**
     * Konstruktor
     *
     * @param terrainShader    TerrainShader
     * @param projectionMatrix Matrix4f
     */
    public TerrainRenderer(TerrainShader terrainShader, Matrix4f projectionMatrix) {
        this.terrainShader = terrainShader;
        terrainShader.start();
        terrainShader.loadProjMatrix(projectionMatrix);
        terrainShader.connectTextureUnits();
        terrainShader.stop();
    }

    /**
     * Vykreslí jednotlivé terény
     *
     * @param terrainList List
     */
    public void render(List<Terrain> terrainList) {
        for (Terrain terrain : terrainList) {
            prepareTerrainModel(terrain);
            loadModelMatrix(terrain);
            GL11.glDrawElements(GL11.GL_TRIANGLES, terrain.getModel().getVertexCount(), GL11.GL_UNSIGNED_INT, 0);

            unbindTerrainModel();
        }
    }

    /**
     * Připravý model terénu a povolí VAO
     *
     * @param terrain Terrain
     */
    private void prepareTerrainModel(Terrain terrain) {
        RawModel rawModel = terrain.getModel();
        GL30.glBindVertexArray(rawModel.getVaoID());
        GL20.glEnableVertexAttribArray(0);
        GL20.glEnableVertexAttribArray(1);
        GL20.glEnableVertexAttribArray(2);

        bindTextures(terrain);
    }

    /**
     * Nabindování všech textur terénu - používáme blend mapu - terén může mít mix textur
     *
     * @param terrain
     */
    private void bindTextures(Terrain terrain) {
        TerrainTexturePack texturePack = terrain.getTexturePack();

        // Hlavní textura
        GL13.glActiveTexture(GL13.GL_TEXTURE0);
        GL11.glBindTexture(GL11.GL_TEXTURE_2D, texturePack.getBackgroundTexture().getTextureID());

        // Textura pro červenou složku blend mapy
        GL13.glActiveTexture(GL13.GL_TEXTURE1);
        GL11.glBindTexture(GL11.GL_TEXTURE_2D, texturePack.getrTexture().getTextureID());

        // Textura pro zelenou složku blend mapy
        GL13.glActiveTexture(GL13.GL_TEXTURE2);
        GL11.glBindTexture(GL11.GL_TEXTURE_2D, texturePack.getgTexture().getTextureID());

        // Textura pro modrou složku blend mapy
        GL13.glActiveTexture(GL13.GL_TEXTURE3);
        GL11.glBindTexture(GL11.GL_TEXTURE_2D, texturePack.getbTexture().getTextureID());

        // Blend mapa
        GL13.glActiveTexture(GL13.GL_TEXTURE4);
        GL11.glBindTexture(GL11.GL_TEXTURE_2D, terrain.getBlendMap().getTextureID());

    }

    /**
     * Odbindování modelu z VAO
     */
    private void unbindTerrainModel() {
        GL20.glDisableVertexAttribArray(0);
        GL20.glDisableVertexAttribArray(1);
        GL20.glDisableVertexAttribArray(2);
        GL30.glBindVertexArray(0);
    }

    /**
     * Nahrání transformační matice
     *
     * @param terrain Terrain
     */
    private void loadModelMatrix(Terrain terrain) {
        Matrix4f tansMatrix = Maths.createTransMatrix(new Vector3f(terrain.getX(), 0, terrain.getZ()), 0, 0, 0, 1);
        terrainShader.loadTransmatrix(tansMatrix);
    }
}
