package cz.filek.zpg.engine;

import cz.filek.zpg.models.RawModel;
import cz.filek.zpg.textures.TextureData;
import cz.filek.zpg.textures.TextureLoader;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.*;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.List;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL13.*;
import static org.lwjgl.opengl.GL12.*;

/**
 * @author Filip Jani - 1. 11. 2016.
 *         Třída sloužící k nahrávání Entit do VAO - Vertex Array Object
 */
public class Loader {
    private List<Integer> vaos = new ArrayList<Integer>();
    private List<Integer> vbos = new ArrayList<Integer>();
    private List<Integer> textures = new ArrayList<Integer>();

    /**
     * Nahraje model do VAO
     *
     * @param positions     pole s pozicemi jednotlivých vrcholů modelu
     * @param textureCoords pole se souřadnicemi pro textury
     * @param normals       pole s normálami
     * @param indices       pole indícií -> pořadí v jakém se mají vrcholy použít
     * @param width         šířka modelu
     * @param height        výška modelu
     * @return RawModel rawModel
     */
    public RawModel loadToVAO(float[] positions, float[] textureCoords, float[] normals, int[] indices, float width, float height) {
        int vaoID = createVAO();
        bindIndicesBuffer(indices);
        storeDataInAtrList(0, 3, positions);
        storeDataInAtrList(1, 2, textureCoords);
        storeDataInAtrList(2, 3, normals);
        unbindVAO();
        return new RawModel(vaoID, indices.length, width, height);
    }

    /**
     * Nahraje model do VAO
     *
     * @param positions  pozice vrcholů
     * @param dimensions dimenze prostoru
     * @return
     */
    public RawModel loadToVAO(float[] positions, int dimensions) {
        int vaoID = createVAO();
        storeDataInAtrList(0, dimensions, positions);
        unbindVAO();
        return new RawModel(vaoID, positions.length / dimensions, 0, 0);
    }

    /**
     * Nahraje texturu do OpenGL a vrátí její ID
     *
     * @param fileName název textury ze složky resources/textures
     * @return ID textury v OpenGL
     */
    public int loadTexture(String fileName) {
        TextureData textureData = TextureLoader.loadPNGTexture(fileName);
        int textureID = TextureLoader.bindTexture(textureData.getWidth(), textureData.getHeight(), textureData.getBuffer());
        textures.add(textureID);
        return textureID;
    }

    /**
     * Metoda vytvářející skybox jako GL_TEXTURE_CUBE_MAP
     *
     * @param skyBoxArray souřadnice jednotlivých vrcholů krychle k vykreslení skyboxu
     * @return vrací ID textury
     */
    public int loadSkyBox(String[] skyBoxArray) {
        int textureID = GL11.glGenTextures();
        GL13.glActiveTexture(GL13.GL_TEXTURE0);
        GL11.glBindTexture(GL_TEXTURE_CUBE_MAP, textureID);

        for (int i = 0; i < skyBoxArray.length; i++) {
            TextureData textureData = TextureLoader.loadPNGTexture(skyBoxArray[i]);
            GL11.glTexImage2D(GL13.GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGBA, textureData.getWidth(), textureData.getHeight(), 0, GL_RGBA, GL11.GL_UNSIGNED_BYTE, textureData.getBuffer());
        }

        glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
        textures.add(textureID);
        return textureID;
    }

    /**
     * Metoda vytvářející VAO
     *
     * @return int VAO ID
     */
    private int createVAO() {
        int vaoID = GL30.glGenVertexArrays();
        vaos.add(vaoID);
        GL30.glBindVertexArray(vaoID);
        return vaoID;
    }

    /**
     * Bindování indícií
     *
     * @param indices indices
     */
    private void bindIndicesBuffer(int[] indices) {
        int vboID = GL15.glGenBuffers();
        vbos.add(vboID);
        GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, vboID);
        IntBuffer intBuffer = storeDataInIntBuffer(indices);
        GL15.glBufferData(GL15.GL_ELEMENT_ARRAY_BUFFER, intBuffer, GL15.GL_STATIC_DRAW);
    }

    /**
     * Ukládání dat do IntBufferu
     *
     * @param data int array
     * @return IntBuffer
     */
    private IntBuffer storeDataInIntBuffer(int[] data) {
        IntBuffer intBuffer = BufferUtils.createIntBuffer(data.length);
        intBuffer.put(data);
        intBuffer.flip();
        return intBuffer;
    }

    /**
     * Ukládání dat do Array Bufferu
     *
     * @param attributeNumber ID atributu
     * @param coordinateSize  dimenze
     * @param data            data
     */
    private void storeDataInAtrList(int attributeNumber, int coordinateSize, float[] data) {
        int vboID = GL15.glGenBuffers();
        vbos.add(vboID);
        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vboID);
        FloatBuffer floatBuffer = storeDataInFloatBuf(data);
        GL15.glBufferData(GL15.GL_ARRAY_BUFFER, floatBuffer, GL15.GL_STATIC_DRAW);
        GL20.glVertexAttribPointer(attributeNumber, coordinateSize, GL11.GL_FLOAT, false, 0, 0);
        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, 0);

    }

    /**
     * Odbindování VAO
     */
    private void unbindVAO() {
        GL30.glBindVertexArray(0);
    }

    /**
     * Uložení dat to FloatBufferu
     *
     * @param data float data
     * @return FloatBuffer
     */
    private FloatBuffer storeDataInFloatBuf(float[] data) {
        FloatBuffer buffer = BufferUtils.createFloatBuffer(data.length);
        buffer.put(data);
        buffer.flip();
        return buffer;
    }

    /**
     * Metoda sloužící k odstranění všech VAO, VBO a textur z paměti
     */
    public void cleanUp() {
        for (int vao : vaos) {
            GL30.glDeleteVertexArrays(vao);
        }
        for (int vbo : vbos) {
            GL15.glDeleteBuffers(vbo);
        }
        for (int texture : textures) {
            GL11.glDeleteTextures(texture);
        }
    }
}
