package cz.filek.zpg.engine;


import cz.filek.zpg.math.Vector2f;
import cz.filek.zpg.math.Vector3f;
import cz.filek.zpg.models.RawModel;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Filip Jani - 2. 11. 2016.
 *         <p>
 *         Třída zajišťující načítání OBJ modelů, modely musejí splňovat určité podmínky aby se dali v pořádku nahrát
 *         <p>
 *         1) Model je vyexportovaný z programu Blender3D ve formátu wavefront
 *         2) Při exportu je zapnut smooth shading
 *         3) Model má přiřazené vt souřadnice
 *         4) Zaškrtnuto Apply Modifiers, Include Edges, Write Normals, Include UVs, Triangulate Faces při exportu
 */
public class OBJLoader {

    /**
     * Načte OBJ soubor ze složky resources/models/
     * - vytvoří 4 pole:
     * 1) Pole vrcholů
     * 2) Pole normál
     * 3) Pole souřadnic pro textury
     * 4) Pole indícií
     *
     * @param fileName Název modelu
     * @param loader   Loader
     * @return RawModel rawModel
     */
    public static RawModel loadObjModel(String fileName, Loader loader) {
        FileReader fr = null;
        InputStreamReader inputStreamReader = null;
        try {
            String modelPath = "/models/" + fileName + ".obj";
            InputStream inputStream = OBJLoader.class.getResourceAsStream(modelPath);
            inputStreamReader = new InputStreamReader(inputStream);

        } catch (Exception ex) {

        }
        if (inputStreamReader != null) {
            BufferedReader reader = new BufferedReader(inputStreamReader);
            String line;
            List<Vector3f> vertices = new ArrayList<Vector3f>();
            List<Vector2f> textures = new ArrayList<Vector2f>();
            List<Vector3f> normals = new ArrayList<Vector3f>();
            List<Integer> indices = new ArrayList<Integer>();
            float[] verticesArray = null;
            float[] normalsArray = null;
            float[] textureArray = null;
            int[] indicesArray = null;
            try {

                // Čtení jednotlivých řádek souboru
                while (true) {
                    line = reader.readLine();
                    String[] currentLine = line.split(" ");
                    // Pokud začíná na v - přidáme do pole vrcholů
                    if (line.startsWith("v ")) {
                        Vector3f vertex = new Vector3f(Float.parseFloat(currentLine[1]),
                                Float.parseFloat(currentLine[2]), Float.parseFloat(currentLine[3]));
                        vertices.add(vertex);
                    }
                    // Pokud začíná na vt - přidáme do pole souřadnic textur
                    else if (line.startsWith("vt ")) {
                        Vector2f texture = new Vector2f(Float.parseFloat(currentLine[1]),
                                Float.parseFloat(currentLine[2]));
                        textures.add(texture);
                    }
                    // Pokud začíná na vn - přidáme do pole normál
                    else if (line.startsWith("vn ")) {
                        Vector3f normal = new Vector3f(Float.parseFloat(currentLine[1]),
                                Float.parseFloat(currentLine[2]), Float.parseFloat(currentLine[3]));
                        normals.add(normal);
                    }
                    // Pokud dojdeme k f inicializujeme pole textur a normál
                    else if (line.startsWith("f ")) {
                        textureArray = new float[vertices.size() * 2];
                        normalsArray = new float[vertices.size() * 3];
                        break;
                    }
                }

                // Načítání faceů (f vertexID/textureCoordID/normalID vertexID/textureCoordID/normalID vertexID/textureCoordID/normalID) - jeden trojůhelník
                while (line != null) {
                    if (!line.startsWith("f ")) {
                        line = reader.readLine();
                        continue;
                    }
                    // Načteme jednotlivé hodnoty
                    String[] currentLine = line.split(" ");
                    String[] vertex1 = currentLine[1].split("/");
                    String[] vertex2 = currentLine[2].split("/");
                    String[] vertex3 = currentLine[3].split("/");

                    // Zpracujeme jednotlivé vrcholy
                    processVertex(vertex1, indices, textures, normals, textureArray, normalsArray);
                    processVertex(vertex2, indices, textures, normals, textureArray, normalsArray);
                    processVertex(vertex3, indices, textures, normals, textureArray, normalsArray);
                    line = reader.readLine();
                }
                reader.close();

            } catch (Exception e) {
                e.printStackTrace();
            }

            verticesArray = new float[vertices.size() * 3];
            indicesArray = new int[indices.size()];

            // Pomocné proměné pro výpočet šířky a délky objektu
            float minX = Float.MAX_VALUE;
            float maxX = Float.MIN_VALUE;
            float minZ = Float.MAX_VALUE;
            float maxZ = Float.MIN_VALUE;

            int vertexPointer = 0;
            for (Vector3f vertex : vertices) {
                if (vertex.x < minX) {
                    minX = vertex.x;
                }
                if (vertex.x > maxX) {
                    maxX = vertex.x;
                }
                if (vertex.z < minZ) {
                    minZ = vertex.z;
                }
                if (vertex.z > maxZ) {
                    maxZ = vertex.z;
                }
                verticesArray[vertexPointer++] = vertex.x;
                verticesArray[vertexPointer++] = vertex.y;
                verticesArray[vertexPointer++] = vertex.z;
            }

            for (int i = 0; i < indices.size(); i++) {
                indicesArray[i] = indices.get(i);
            }
            float width = maxX - minX;
            float height = maxZ - minZ;
            return loader.loadToVAO(verticesArray, textureArray, normalsArray, indicesArray, width, height);
        }
        return new RawModel(0, 0, 0, 0);
    }

    /**
     * Zpracuje jednotlivé vrcholy - převede jednotlivé listy vektorů do pole, které se používá v OpenGL drawArrays...
     *
     * @param vertexData   vertex data (vertexID/textCoordsID/normalID)
     * @param indices      list indícií
     * @param textures     list Vectorů textur
     * @param normals      list Vectorů normál
     * @param textureArray pole textur
     * @param normalsArray pole normál
     */
    private static void processVertex(String[] vertexData, List<Integer> indices,
                                      List<Vector2f> textures, List<Vector3f> normals, float[] textureArray,
                                      float[] normalsArray) {
        int currentVertexPointer = Integer.parseInt(vertexData[0]) - 1;
        indices.add(currentVertexPointer);
        Vector2f currentTex = textures.get(Integer.parseInt(vertexData[1]) - 1);
        textureArray[currentVertexPointer * 2] = currentTex.x;
        textureArray[currentVertexPointer * 2 + 1] = 1 - currentTex.y;
        Vector3f currentNorm = normals.get(Integer.parseInt(vertexData[2]) - 1);
        normalsArray[currentVertexPointer * 3] = currentNorm.x;
        normalsArray[currentVertexPointer * 3 + 1] = currentNorm.y;
        normalsArray[currentVertexPointer * 3 + 2] = currentNorm.z;
    }

}