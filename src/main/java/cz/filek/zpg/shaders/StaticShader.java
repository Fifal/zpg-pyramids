package cz.filek.zpg.shaders;

import cz.filek.zpg.entities.Camera;
import cz.filek.zpg.entities.Light;
import cz.filek.zpg.math.Maths;
import cz.filek.zpg.math.Matrix4f;
import cz.filek.zpg.math.Vector3f;

/**
 * @author Filip Jani -  1. 11. 2016.
 *         <p>
 *         Shader pro entity
 */
public class StaticShader extends ShaderProgram {
    private static final String VERTEX_FILE = "/shaders/vertexShader.vs";
    private static final String FRAGMENT_FILE = "/shaders/fragmentShader.vs";

    private int locTransMatrix;
    private int locProjMatrix;
    private int locViewMatrix;
    private int locLightPosition;
    private int locLightColor;
    private int locFakeLighting;
    private int locSkyColor;

    public StaticShader() {
        super(VERTEX_FILE, FRAGMENT_FILE);
    }

    @Override
    protected void getAllUniformLocations() {
        locTransMatrix = super.getUniformLocation("transMatrix");
        locProjMatrix = super.getUniformLocation("projMatrix");
        locViewMatrix = super.getUniformLocation("viewMatrix");
        locLightColor = super.getUniformLocation("lightColor");
        locLightPosition = super.getUniformLocation("lightPosition");
        locFakeLighting = super.getUniformLocation("useFakeLighting");
        locSkyColor = super.getUniformLocation("skyColor");
    }

    @Override
    protected void bindAttributes() {
        super.bindAttribute(0, "position");
        super.bindAttribute(1, "textureCoords");
        super.bindAttribute(2, "normal");
    }

    public void loadTransmatrix(Matrix4f matrix4f) {
        super.loadMatrix(locTransMatrix, matrix4f);
    }

    public void loadProjMatrix(Matrix4f projection) {
        super.loadMatrix(locProjMatrix, projection);
    }

    public void loadViewMatrix(Camera camera) {
        Matrix4f viewMatrix = Maths.createViewMatrix(camera);
        super.loadMatrix(locViewMatrix, viewMatrix);
    }

    public void loadFakeLighting(boolean useFake) {
        super.loadBoolean(locFakeLighting, useFake);
    }

    public void loadLight(Light light) {
        super.loadVector(locLightColor, light.getColor());
        super.loadVector(locLightPosition, light.getPosition());
    }

    public void loadSkyColor(float r, float g, float b) {
        super.loadVector(locSkyColor, new Vector3f(r, g, b));
    }

}
