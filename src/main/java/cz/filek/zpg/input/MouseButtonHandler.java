package cz.filek.zpg.input;

import cz.filek.zpg.engine.DisplayManager;
import org.lwjgl.glfw.GLFWMouseButtonCallback;

import static org.lwjgl.glfw.GLFW.GLFW_RELEASE;
import static org.lwjgl.glfw.GLFW.glfwGetMouseButton;

/**
 * @author Filip Jani - 7. 11. 2016.
 *         <p>
 *         Třída pro zachytávání tlačítek myši
 */
public class MouseButtonHandler extends GLFWMouseButtonCallback {
    // Pole pro tlačítka
    private static boolean[] keys = new boolean[65536];

    @Override
    public String getSignature() {
        return null;
    }

    @Override
    public void callback(long args) {
    }

    @Override
    public void invoke(long window, int button, int action, int mods) {
        keys[button] = action != GLFW_RELEASE;
    }

    @Override
    public void close() {

    }

    /**
     * Zjištění jestli je tlačítko stisknuto
     *
     * @param button GLFW kód tlačítka
     * @return true pokud je stisknuto
     */
    public static boolean isButtonDown(int button) {
        if (glfwGetMouseButton(DisplayManager.getWindowID(), button) == 1) {
            return true;
        } else {
            return false;
        }
    }
}
