package cz.filek.zpg.models;

import cz.filek.zpg.textures.ModelTexture;

/**
 * @author Filip Jani - 1. 11. 2016.
 *         <p>
 *         Třída uchovávající dvojici RawModel a jeho texturu ModelTexture
 */
public class TexturedModel {
    private RawModel rawModel;
    private ModelTexture modelTexture;

    /**
     * Konstruktor
     *
     * @param rawModel     RawModel
     * @param modelTexture ModelTexture
     */
    public TexturedModel(RawModel rawModel, ModelTexture modelTexture) {
        this.rawModel = rawModel;
        this.modelTexture = modelTexture;
    }

    /**
     * Vrací RawModel
     *
     * @return RawModel
     */
    public RawModel getRawModel() {
        return rawModel;
    }

    /**
     * Vrací ModelTexture
     *
     * @return ModelTexture
     */
    public ModelTexture getModelTexture() {
        return modelTexture;
    }
}
