package cz.filek.zpg.terrains;

import cz.filek.zpg.engine.Loader;
import cz.filek.zpg.math.Maths;
import cz.filek.zpg.math.Vector2f;
import cz.filek.zpg.math.Vector3f;
import cz.filek.zpg.models.RawModel;
import cz.filek.zpg.textures.TerrainTexture;
import cz.filek.zpg.textures.TerrainTexturePack;

import java.awt.image.BufferedImage;

/**
 * @author Filip Jani - 5. 11. 2016.
 *         <p>
 *         Třída generující terén
 */
public class Terrain {
    // Velikost terénu
    private static final float SIZE = 768;
    // Maximální výška terénu
    private static final float MAX_HEIGHT = 100;
    // Počet pixelů
    private static final float MAX_PIXEL_COUNT = 256 * 256 * 256;

    private float x;
    private float z;

    private RawModel model;
    private TerrainTexturePack texturePack;
    private TerrainTexture blendMap;
    private BufferedImage heightMap;
    private float[][] heightMapArray;

    /**
     * Konstruktor
     *
     * @param gridX     pozice ve scéně X
     * @param gridZ     pozice ve scéně Z
     * @param loader    Loader
     * @param texture   Textury terénu
     * @param blendMap  Blend mapa
     * @param heightMap Výšková mapa
     */
    public Terrain(float gridX, float gridZ, Loader loader, TerrainTexturePack texture, TerrainTexture blendMap, BufferedImage heightMap) {
        this.texturePack = texture;
        this.blendMap = blendMap;
        this.x = gridX * SIZE;
        this.z = gridZ * SIZE;
        this.heightMap = heightMap;
        this.model = generateTerrain(loader);
    }

    /**
     * Vygeneruje terén jako RawModel
     *
     * @param loader loader
     * @return RawModel terénu
     */
    private RawModel generateTerrain(Loader loader) {
        int VERTEX_COUNT = heightMap.getHeight();
        this.heightMapArray = new float[VERTEX_COUNT][VERTEX_COUNT];

        int count = VERTEX_COUNT * VERTEX_COUNT;
        float[] vertices = new float[count * 3];
        float[] normals = new float[count * 3];
        float[] textureCoords = new float[count * 2];
        int[] indices = new int[6 * (VERTEX_COUNT - 1) * (VERTEX_COUNT - 1)];
        int vertexPointer = 0;
        for (int i = 0; i < VERTEX_COUNT; i++) {
            for (int j = 0; j < VERTEX_COUNT; j++) {
                vertices[vertexPointer * 3] = (float) j / ((float) VERTEX_COUNT - 1) * SIZE;
                float height = getHeight(j, i);
                heightMapArray[j][i] = height;
                vertices[vertexPointer * 3 + 1] = height;
                vertices[vertexPointer * 3 + 2] = (float) i / ((float) VERTEX_COUNT - 1) * SIZE;
                normals[vertexPointer * 3] = calculateNormal(j, i).x;
                normals[vertexPointer * 3 + 1] = calculateNormal(j, i).y;
                normals[vertexPointer * 3 + 2] = calculateNormal(j, i).z;
                textureCoords[vertexPointer * 2] = (float) j / ((float) VERTEX_COUNT - 1);
                textureCoords[vertexPointer * 2 + 1] = (float) i / ((float) VERTEX_COUNT - 1);
                vertexPointer++;
            }
        }
        int pointer = 0;
        for (int gz = 0; gz < VERTEX_COUNT - 1; gz++) {
            for (int gx = 0; gx < VERTEX_COUNT - 1; gx++) {
                int topLeft = (gz * VERTEX_COUNT) + gx;
                int topRight = topLeft + 1;
                int bottomLeft = ((gz + 1) * VERTEX_COUNT) + gx;
                int bottomRight = bottomLeft + 1;
                indices[pointer++] = topLeft;
                indices[pointer++] = bottomLeft;
                indices[pointer++] = topRight;
                indices[pointer++] = topRight;
                indices[pointer++] = bottomLeft;
                indices[pointer++] = bottomRight;
            }
        }
        return loader.loadToVAO(vertices, textureCoords, normals, indices, 0, 0);
    }

    /**
     * Vrací X souřadnici terénu
     *
     * @return float
     */
    public float getX() {
        return x;
    }

    /**
     * Vrací Z souřadnici terénu
     *
     * @return float
     */
    public float getZ() {
        return z;
    }

    /**
     * Vrací RawModel terénu
     *
     * @return RawModel
     */
    public RawModel getModel() {
        return model;
    }

    /**
     * Vrací TexturePack pro terén
     *
     * @return TerrainTexturePack
     */
    public TerrainTexturePack getTexturePack() {
        return texturePack;
    }

    /**
     * Vrací Blendmapu terénu
     *
     * @return TerrainTexture
     */
    public TerrainTexture getBlendMap() {
        return blendMap;
    }

    /**
     * Výpočet normál pro terén
     *
     * @param x xová souřadnice
     * @param z zová souřadnice
     * @return Vector3f normála
     */
    private Vector3f calculateNormal(int x, int z) {
        if (heightMap != null) {
            float heightL = getHeight(x - 1, z);
            float heightR = getHeight(x + 1, z);
            float heightD = getHeight(x, z - 1);
            float heightU = getHeight(x, z + 1);
            return new Vector3f(heightL - heightR, 2f, heightD - heightU);
        }
        return new Vector3f(0.0f, 2f, 0.0f);
    }

    /**
     * Vrací výšku terénu v daném bodě výškové mapy
     *
     * @param x xová souřadníce ve scéně
     * @param z zová souřadnice ve scéně
     * @return float
     */
    private float getHeight(int x, int z) {
        if (heightMap != null) {
            if (x > 0 && x < heightMap.getHeight() && z > 0 && z < heightMap.getHeight()) {
                float height = heightMap.getRGB(x, z);
                height += MAX_PIXEL_COUNT / 2f;
                height /= MAX_PIXEL_COUNT / 2f;
                height *= MAX_HEIGHT;
                return height;
            }
        }
        return 0;
    }

    /**
     * Vrací výšku terénu v daném bodě ve scéně
     *
     * @param worldX xová souřadnice
     * @param worldZ zová souřadnice
     * @return float
     */
    public float getHeightOfTerrain(float worldX, float worldZ) {
        float terrainX = worldX - this.x;
        float terrainZ = worldZ - this.z;
        float gridSquareSize = SIZE / ((float) heightMapArray.length - 1);
        int gridX = (int) Math.floor(terrainX / gridSquareSize);
        int gridZ = (int) Math.floor(terrainZ / gridSquareSize);
        if (gridX >= heightMapArray.length - 1 || gridZ >= heightMapArray.length - 1 || gridX < 0 || gridZ < 0) {
            return 0;
        }
        float xCoord = (terrainX % gridSquareSize / gridSquareSize);
        float zCoord = (terrainZ % gridSquareSize / gridSquareSize);
        float answer;
        if (xCoord <= (1 - zCoord)) {
            answer = Maths.barryCentric(new Vector3f(0, heightMapArray[gridX][gridZ], 0), new Vector3f(1,
                    heightMapArray[gridX + 1][gridZ], 0), new Vector3f(0,
                    heightMapArray[gridX][gridZ + 1], 1), new Vector2f(xCoord, zCoord));
        } else {
            answer = Maths.barryCentric(new Vector3f(1, heightMapArray[gridX + 1][gridZ], 0), new Vector3f(1,
                    heightMapArray[gridX + 1][gridZ + 1], 1), new Vector3f(0,
                    heightMapArray[gridX][gridZ + 1], 1), new Vector2f(xCoord, zCoord));
        }
        return answer;
    }
}
